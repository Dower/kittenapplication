import { StyleSheet } from 'react-native';


export const card = StyleSheet.create({
    window: {
        backgroundColor: '#afafaf',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 10,
        margin: 5,
        alignItems: 'center',
    },
    image: {
        width: '95%',
        height: 200,
        margin: 5,
        backgroundColor: '#cfe2f3',
    },
    title: {
        width: '100%',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 17,
        marginVertical: 5,
    },
});
      
export const filters = StyleSheet.create({
    button: {
        width: '29%',
        backgroundColor: '#cfe2f3',
        textAlign: 'center',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
    },
    btnText: {
        fontWeight: 'bold',
        fontSize: 15,
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        width: '50%',
        margin: 10
    }
});

export const view = StyleSheet.create({
    image: {
        aspectRatio: 300 / 200, 
        width: '100%',
    },
    title: {
        fontSize: 25,
    },
    description: {
        fontSize: 17,
        marginTop: 10,
        marginHorizontal: 40,
    }
});

export const global = StyleSheet.create({
    flex: {
        flex: 1,
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#7373733b',
        backgroundColor: 'rgba(100, 100, 100, 0.5)'
    },
    center: {
        alignItems: 'center',
    },
    title: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 17,
        padding: 10,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    content: {
        padding: 10,
    },
    container: {
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        paddingBottom: 20,
        margin: 5
    },
});

