import React from 'react';
import { Text, View, TouchableOpacity, TextInput, Button } from 'react-native';
import { global, filters } from '../styles/styles';
import { Formik } from 'formik';

export default function Filters({ switchFilter, closeModal }){

    return (

        <View style={global.window}>
            <View style = {global.container}>
                <View style = {global.center}>
                    <Text style={global.title}>Filters</Text>
                </View>
                <View style = {[global.content, global.row]}>
                    <TouchableOpacity style = {filters.button} onPress = {() => switchFilter(30)}>
                        <Text style = {filters.btnText}>30</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style = {filters.button} onPress = {() => switchFilter(50)}>
                        <Text style = {filters.btnText}>50</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style = {filters.button} onPress = {() => switchFilter(100000)}>
                        <Text style = {filters.btnText}>100</Text>
                    </TouchableOpacity>
                </View>
            </View>

            <View style = {global.container}>
                <Formik
                    initialValues = {{ count: '' }}
                    onSubmit = {(values) => {
                        if(values.count && values.count>0) switchFilter(values.count);
                    }}
                >
                    {(props) => (
                        <View style = {global.center}>
                            <Text style={global.title}>Custom filter</Text>
                            <TextInput style = {filters.input}
                                placeholder = 'Count'
                                textAlign={'center'}
                                keyboardType = 'numeric'
                                onChangeText = {props.handleChange('count')}
                                value = {props.values.count}
                            />
                            <TouchableOpacity style = {filters.button} onPress={props.handleSubmit}>
                                <Text style = {filters.btnText}>submit</Text>
                            </TouchableOpacity>
                        </View>
                    )}
                </Formik>
            </View>
            <Button title='back' onPress={() => {closeModal()}} />
        </View>
    );
}