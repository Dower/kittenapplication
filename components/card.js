import React from 'react';
import { Text, Image, TouchableOpacity, View } from 'react-native';
import { global, card } from '../styles/styles';


export default function Card({ item, viewItem }){

	return (
		<TouchableOpacity style = {card.window} onPress={() => viewItem(item)}>
				<Image 
					style = {card.image}
					source = {{ uri: (item.img) }}
					resizeMode = "contain"
				/>
			<Text style = {card.title}>{item.name}</Text>
		</TouchableOpacity>
	);
}